<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductController extends Controller
{
    private $serializer;
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/product/{id}", name="product", methods={"GET"})
     */
    public function single(Product $product)
    {
        $json = $this->serializer->serialize($product,"json");
        return JsonResponse::fromJsonString($json);

    }

    /**
     * @Route("/product", name="addProduct", methods={"POST"})
     */
    public function add(Request $request)
    {
        $body = $request->getContent();
        $product= $this->serializer->deserialize($body, Product::class, "json");
        $manager= $this->getDoctrine()->getManager();
        $manager->persist();
        $manager->flush();
        return JsonResponse::fromJsonString(["id" => $product->getId()]);
    }

    /**
     * @Route("/product/{id}", methods={"PATCH"})
     */
    public function update(Product $product)
    {
        $manager= $this->getDoctrine()->getManager();
        $manager->update($product);
        $manager->flush();
        return new Response("", 204);
    }

    /**
     * @Route("/product/{id}", name="removeProduct", methods={"DELETE"})
     */
    public function remove(Product $product)
    {
        $manager= $this->getDoctrine()->getManager();
        $manager->remove($productList);
        $manager->flush();
        return new Response("", 204);
    }
}
