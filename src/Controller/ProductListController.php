<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\ProductList;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class ProductListController extends Controller
{
    private $serializer;
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }
    /**
     * @Route("/productList", name="product_list", methods={"GET"})
     */
    public function index()
    {
        $repo= $this->getDoctrine()->getRepository(ProductList::class);
        $json= $this->serializer->serialize($repo->findAll(), "json");
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/productList/{id}", name="singlePL", methods={"GET"})
     */
    public function single()
    {

    }

    /**
     * @Route("/productList", name="addPL", methods={"POST"})
     */
    public function add(Request $request)
    {
        $body = $request->getContent();
        $productList= $this->serializer->deserialize($body, ProductList::class, "json");
        $manager= $this->getDoctrine()->getManager();
        $manager->persist();
        $manager->flush();
        return JsonResponse::fromJsonString(["id" => $productList->getId()]);
    }

    /**
     * @Route("/productList/{id}", name="updatePL", methods={"PATCH"})
     */
    public function update(ProductList $productList, Request $request)
    {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, ProductList::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $student->setName($updated->getName());
        $student->setSurname($updated->getSurname());
        $student->setLevel($updated->getLevel());
        $student->setTech($updated->getTech());
        
        $manager->flush();
        return new Response("", 204);

    }

    /**
     * @Route("/productList/{id}", name="removePL", methods={"DELETE"})
     */
    public function remove(ProductList $productList)
    {
        $manager= $this->getDoctrine()->getManager();
        $manager->remove($productList);
        $manager->flush();
        return new Response("", 204);
    }
}
